#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>

#include "bc.h"

extern int yydebug;
int yyparse (void);

static FILE * outfile = NULL;

unsigned char gbcd [64] =
  {
    '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '[', '#', '@', ':', '>', '?',
    ' ', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', '&', '.', ']', '(', '<', '\\',
    '^', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', '-', '$', '*', ')', ';', '\'',
    '+', '/', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '_', ',', '%', '=', '"', '!'
  };

struct symbol * symbol_table = NULL;
// Number of arguments in the current function
static struct symbol * current_function = NULL;


#define EMIT_SIZE 4096
static char _emit_buffer [EMIT_SIZE + 1];

static void _emit_start (void)
  {
    _emit_buffer [0] = 0;
  }

static void _emit_line (void)
  {
    fprintf (outfile, "%s\n", _emit_buffer);
    _emit_start ();
  }

static void _emit_string (char * string)
  {
    if (strlen (string) + strlen (_emit_buffer) > EMIT_SIZE)
      {
        fprintf (stderr, "emit buffer overflow; punting\n");
        return;
      }
    strcat (_emit_buffer, string);
  }

static void _emit_tab (int pos)
  {
    size_t l = strlen (_emit_buffer);
    int d = pos - (int) l;
    while (d > 0)
      {
        strcat (_emit_buffer, " ");
        d --;
      }
  }

void emit_function_entry (void)
  {
    if (! current_function)
      {
        fprintf (stderr, "emit_function_entry() called with null current_function.\n");
        exit (1);
       }
    _emit_start ();
    _emit_tab (8);
    _emit_string ("SYMDEF");
    _emit_tab (32);
    _emit_string (current_function->name);
    _emit_line ();

    _emit_string (current_function->name);
    _emit_line ();

    _emit_tab (8);
    _emit_string ("SAVE");
    _emit_line ();
  }

// string to word
// assuming unsigned long long is >= 36 bits

word strtoword (char * string)
  {
    char * endptr;
    unsigned long long v = strtoull (string, & endptr, 0);
    if (! string || !*string)
      {
        fprintf (stderr, "bad string passed to strtoword(); punting with 0\n");
        return 0;
      }
    if (* endptr)
      {
        fprintf (stderr, "invalid digit '%c'; truncating.\n", * endptr);
      }
    if (v > 0777777777777ull)
      {
        fprintf (stderr, "'%s' too big; clipping.\n", string);
        v &= 0777777777777ull;
      }
    return (word) v;
  }

// If local, search only the local symbols

struct symbol * lookup (char * name, bool local)
  {
    /* Search the local symbol table if in a function definition */
    if (current_function)
      {
        struct symbol * p = current_function->locals;
        while (p)
          {
            if (strcmp (p -> name, name) == 0)
              return p;
            p = p->next;
          }
      }
    /* Search the global symbol table */
    struct symbol * p = symbol_table;
    while (p)
      {
        if (strcmp (p -> name, name) == 0)
          return p;
        p = p->next;
      }
    return NULL;
  }

void define_manifest (char * name, char * value)
  {
    //fprintf (stderr, "define_manifest '%s' = '%s'\n", name, value);
    if (lookup (name, false))
      {
        fprintf (stderr, "manifest '%s' is already defined\n", name);
        return;
      }
    struct symbol * p = malloc (sizeof (struct symbol));
    if (! p)
      {
        fprintf (stderr, "out of memory in define_manifest\n");
        exit (1);
      }
    memset (p, 0, sizeof (* p));
    // strip trailing semicolon
    ssize_t l = strlen (value);
    if (l && value [l - 1] == ';')
      value [l - 1] = 0;
    p->name = name;
    p->svalue = value;
    p->type = symbol_manifest;
    p->next = symbol_table;
    symbol_table = p;
  }


void define_function (char * name)
  {
    if (lookup (name, false))
      {
        fprintf (stderr, "function '%s' is already defined\n", name);
        return;
      }
    struct symbol * p = malloc (sizeof (struct symbol));
    if (! p)
      {
        fprintf (stderr, "out of memory in define_function\n");
        exit (1);
      }
    p->name = name;
    p->type = symbol_function;
    p->locals = NULL;
    p->next = symbol_table;
    p->nargs = 0;
    p->nautos = 0;
    symbol_table = p;
    current_function = p;
  }

void close_function (void)
  {
    current_function = NULL;
  }

void define_argument (char * name)
  {
    if (! current_function)
      {
        fprintf (stderr, "internal error; define_argument() called with NULL current_function\n");
        exit (1);
      }

    // Only search the local symbol table; locals can redefine globals
    if (lookup (name, true))
      {
        fprintf (stderr, "argument '%s' is already defined\n", name);
        return;
      }
    struct symbol * p = malloc (sizeof (struct symbol));
    if (! p)
      {
        fprintf (stderr, "out of memory in define_function\n");
        exit (1);
      }
    p->name = name;
    p->type = symbol_argument;
    p->arg_number = current_function->nargs ++;
    p->locals = NULL;
    p->next = current_function->locals;
    current_function->locals = p;
  }

void define_auto (char * name, word length)
  {
    if (! current_function)
      {
        fprintf (stderr, "internal error; define_auto() called with NULL current_function\n");
        exit (1);
      }

    // Only search the local symbol table; locals can redefine globals
    if (lookup (name, true))
      {
        fprintf (stderr, "auto '%s' is already defined\n", name);
        return;
      }
    struct symbol * p = malloc (sizeof (struct symbol));
    if (! p)
      {
        fprintf (stderr, "out of memory in define_auto\n");
        exit (1);
      }
    p->name = name;
    p->type = symbol_auto;
    p->auto_number = current_function->nautos ++;
    p->auto_length = length;
    p->auto_offset = current_function->auto_offset;
    current_function->auto_offset += length;
    p->locals = NULL;
    p->next = current_function->locals;
    current_function->locals = p;
  }

void print_symbols_p (struct symbol * p)
  {
    if (! p)
      return;
    if (p->next)
      print_symbols_p (p->next);
    switch (p->type)
      {
        case symbol_manifest:
          fprintf (stderr, "manifest %s: '%s'\n", p->name, p->svalue);
          break;
        case symbol_function:
          fprintf (stderr, "function %s (%d)\n", p->name, p->nargs);
          print_symbols_p (p->locals);
          break;
        case symbol_argument:
          fprintf (stderr, "argument %s: number %d\n", p->name, p->arg_number);
          break;
        case symbol_auto:
          fprintf (stderr, "auto     %s: number %d, length %lld, offset %lld\n", p->name, p->auto_number, p->auto_length, p->auto_offset);
          break;
        default:
          fprintf (stderr, "unknown  %s\n", p->name);
          break;
      }
  }

void print_symbols (void)
  {
    fprintf (stderr, "Symbol table\n");
    print_symbols_p (symbol_table);
  }


int main (int argc, char * argv [])
  {
    outfile = fopen ("bc.gmap", "w");
    yydebug = 1;
    yyparse ();
    print_symbols ();
  }

