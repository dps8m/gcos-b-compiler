all : bc

CFLAGS = -O0 -g

bc : bcompiler.o bc.tab.o lex.yy.o
	clang ${CFLAGS} -o bc bcompiler.o bc.tab.o lex.yy.o

bcompiler.o : bcompiler.c bc.h

bc.tab.o : bc.tab.c bc.tab.h

# '-r all' generates report bc.output
# '-t' debug trace
bc.tab.c bc.tab.h : bc.y
	bison -d bc.y -r all -t

lex.yy.o : lex.yy.c

# -d is debug
lex.yy.c : bc.l bc.tab.h
	flex -d bc.l 

clean : 
	-rm bc.tab.c bc.tab.h bc.tab.o lex.yy.c lex.yy.o bcompiler.o
