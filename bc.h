#define MAX_STR_CONST 4096
#define MAX_CHAR_CONST 4
#define MAX_BCD_CONST 6

typedef uint64_t word;

enum symbol_type {symbol_manifest, symbol_function, symbol_argument, symbol_auto};

struct symbol
  {
    char * name;
    enum symbol_type type;
    word value; // support 36 bit words
    char * svalue;
    struct symbol * locals;
    // type function
    int nargs;
    int nautos; 

    // type argument
    int arg_number;

    // type auto
    int auto_number;
    word auto_length;
    word auto_offset;

    struct symbol * next;
  };
// GE/Honeywell GBCD https://en.wikipedia.org/wiki/BCD_(character_encoding)

unsigned char gbcd [64];

void set_manifest (void);
word strtoword (char * string);
void define_manifest (char * name, char * value);
void close_function (void);
void emit_function_entry (void);
void define_function (char * name);
void define_argument (char * name);
void define_auto (char * name, word length);
