%{

#include "bc.h"
#include "bc.tab.h"
#define append(c) { if (string_buf_ptr == string_buf_limit) fprintf (stderr, "String constant too long\n"); else * string_buf_ptr ++ = c; }

%}

alpha [a-zA-Z_.]
nzdigit [1-9]
digit [0-9]
odigit [0-7]

%x str
%x cconst
%x bcd
%x manifest
%option stack yy_push_state
%option yylineno
%%

          /* character string */
	/* adapted from ftp://ftp.gnu.org/old-gnu/Manuals/flex-2.5.4/html_mono/flex.html */
          // short 'cause of nine bit chars.
	unsigned short string_buf [MAX_STR_CONST];
	unsigned short * string_buf_ptr;
          unsigned short * string_buf_limit;
\"	{
	  string_buf_ptr = string_buf;
	  string_buf_limit = string_buf + MAX_STR_CONST - 1;
	  BEGIN(str);
	}

<str>\"	{ /* saw closing quote - all done */
	  BEGIN(INITIAL);
	  * string_buf_ptr = '\0';
	  /* return string constant token type and * value to parser */
	  // XXX yylval.string = strdup (string_buf);
	  return STRING;
	}

<str>\n	{
	  /* error - unterminated string constant */
	  fprintf (stderr, "Unterminated string constart\n");
	  BEGIN(INITIAL);
	  * string_buf_ptr = '\0';
	  /* return string constant token type and * value to parser */
	  // XXX yylval.string = strdup (string_buf);
	  return STRING;
	}

<str>\*[0-7]{1,3}	{
		  /* octal escape sequence */
		  int result;
		  (void) sscanf (yytext + 1, "%o", & result);
                      // we should be able to specify nine bit values,
                      // but string_buf would have to be reworked
		  //if (result > 0777)
		  if (result > 0377)
		    {
		      /* error, constant is out-of-bounds */
		      fprintf (stderr, "Octal escape too big\n");
                        }
		  else
		    {
                          append (result);
		    }
		}
	
<str>\*e	append (0);
<str>\*\(	append ('{');
<str>\*\)	append ('}');
<str>\*\<	append ('[');
<str>\*\>	append (']');
<str>\*t	append ('\t');
<str>\*\*	append ('*');
<str>\*\'	append ('\'');
<str>\*\"	append ('"');
<str>\*n	append ('\n');
<str>\*r	append ('\r');
<str>\*f	append ('\f');
<str>\*b	append ('\b');
<str>\*v	append ('\v');
<str>\*x	append (0177);

<str>\\(.|\n)	* string_buf_ptr ++ = yytext [1];

<str>[^\\\n\"]+	{
		  char * yptr = yytext;
		  while (* yptr)
		    * string_buf_ptr ++ = * yptr ++;
		}

          /* character constant */
	/* adapted from ftp://ftp.gnu.org/old-gnu/Manuals/flex-2.5.4/html_mono/flex.html */
\'	{
	  string_buf_ptr = string_buf;
	  string_buf_limit = string_buf + MAX_CHAR_CONST - 1;
	  BEGIN(cconst);
	}

<cconst>\'	{ /* saw closing quote - all done */
	  BEGIN(INITIAL);
	  * string_buf_ptr = '\0';
	  //yylval.string = strdup (string_buf);
            yylval.number = 0;
            for (int i = 0; string_buf[i]; i ++)
              {
                 yylval.number << 9;
                 yylval.number |= ((word) (string_buf [i] & 0777));
              }
	  return CCONST;
	}

<cconst>\n	{
	  /* error - unterminated string constant */
	  fprintf (stderr, "Unterminated string constart\n");
	  BEGIN(INITIAL);
	  * string_buf_ptr = '\0';
	  /* return string constant token type and * value to parser */
            yylval.number = 0;
            for (int i = 0; string_buf[i]; i ++)
              {
                 yylval.number << 9;
                 yylval.number |= ((word) (string_buf [i] & 0777));
              }
	  return CCONST;
	}

<cconst>\*[0-7]{1,3}	{
		  /* octal escape sequence */
		  int result;
		  (void) sscanf (yytext + 1, "%o", & result);
		  if (result > 0777)
		    {
		      /* error, constant is out-of-bounds */
		      fprintf (stderr, "Octal escape too big\n");
                        }
		  else
		    {
                          append (result);
		    }
		}
	
<cconst>\*e	append (0);
<cconst>\*\(	append ('{');
<cconst>\*\)	append ('}');
<cconst>\*\<	append ('[');
<cconst>\*\>	append (']');
<cconst>\*t	append ('\t');
<cconst>\*\*	append ('*');
<cconst>\*\'	append ('\'');
<cconst>\*\"	append ('"');
<cconst>\*n	append ('\n');
<cconst>\*r	append ('\r');
<cconst>\*f	append ('\f');
<cconst>\*b	append ('\b');
<cconst>\*v	append ('\v');
<cconst>\*x	append (0177);

<cconst>\\(.|\n)	* string_buf_ptr ++ = yytext [1];

<cconst>[^\\\n\']+	{
		  char * yptr = yytext;
		  while (* yptr)
		    * string_buf_ptr ++ = * yptr ++;
		}

          /* bcd constant */
	/* adapted from ftp://ftp.gnu.org/old-gnu/Manuals/flex-2.5.4/html_mono/flex.html */
\`	{
	  string_buf_ptr = string_buf;
	  string_buf_limit = string_buf + MAX_CHAR_CONST - 1;
	  BEGIN(bcd);
	}

<bcd>\`	{ /* saw closing quote - all done */
	  BEGIN(INITIAL);
	  * string_buf_ptr = '\0';
	  /* return string constant token type and * value to parser */
            yylval.number = 0;
            for (int i = 0; string_buf[i]; i ++)
              {
                 unsigned short c = string_buf [i] & 0777;
                 int i;
                 for (i = 0; i < 64; i ++)
                   if (gbcd[i] == c)
                     break;
                 if (i > 63)
                   {
                     fprintf (stderr, "invalid BCD character; substituing '?'\n");
                     i = 15;
                   }
                 yylval.number << 6;
                 yylval.number |= ((word) (string_buf [i] & 0777));
              }
	  return BCD;
	}

<bcd>\n	{
	  /* error - unterminated string constant */
	  fprintf (stderr, "Unterminated BCD constart\n");
	  BEGIN(INITIAL);
	  * string_buf_ptr = '\0';
            yylval.number = 0;
            for (int i = 0; string_buf[i]; i ++)
              {
                 unsigned short c = string_buf [i] & 0777;
                 int i;
                 for (i = 0; i < 64; i ++)
                   if (gbcd[i] == c)
                     break;
                 if (i > 63)
                   {
                     fprintf (stderr, "invalid BCD character; substituing '?'\n");
                     i = 15;
                   }
                 yylval.number << 6;
                 yylval.number |= ((word) (string_buf [i] & 0777));
              }
	  return BCD;
	}

<bcd>\*[0-7]{1,3}	{
		  /* octal escape sequence */
		  int result;
		  (void) sscanf (yytext + 1, "%o", & result);
		  if (result > 0777)
		    {
		      /* error, constant is out-of-bounds */
		      fprintf (stderr, "Octal escape too big\n");
                        }
		  else
		    {
                          append (result);
		    }
		}
	
<bcd>\*e	append (0);
<bcd>\*\(	append ('{');
<bcd>\*\)	append ('}');
<bcd>\*\<	append ('[');
<bcd>\*\>	append (']');
<bcd>\*t	append ('\t');
<bcd>\*\*	append ('*');
<bcd>\*\'	append ('\'');
<bcd>\*\"	append ('"');
<bcd>\*n	append ('\n');
<bcd>\*r	append ('\r');
<bcd>\*f	append ('\f');
<bcd>\*b	append ('\b');
<bcd>\*v	append ('\v');
<bcd>\*x	append (0177);

<bcd>\\(.|\n)	* string_buf_ptr ++ = yytext [1];

<bcd>[^\\\n\`]+	{
		  char * yptr = yytext;
		  while (* yptr)
		    * string_buf_ptr ++ = * yptr ++;
		}

	/* Manifest constant value */
	/* ftp://ftp.gnu.org/old-gnu/Manuals/flex-2.5.4/html_mono/flex.html */

<manifest>.*;	{ yylval.string = strdup (yytext); BEGIN (INITIAL); return MANIFEST; }


[ \t]	;
[ \n]	{ yylineno = yylineno + 1;}
{nzdigit}{digit}*	{ yylval.number = strtoword (yytext); return NUM; }
0{odigit}*	{ yylval.number = strtoword (yytext); return NUM; } /* octal */
"auto"	return AUTO;
"extrn"	return EXTRN;
"if"	return IF;
"else"	return ELSE;
"for"	return FOR;
"while"	return WHILE;
"repeat"	return REPEAT;
"switch"	return SWITCH;
"do"	return DO;
"return"	return RETURN;
"break"	return BREAK;
"goto"	return GOTO;
"next"	return NEXT;
"case"	return CASE;
"default"	return DEFAULT;
{alpha}({alpha}|{digit})*	{yylval.string = strdup (yytext); return NAME; }
"("	return LPAREN;
")"	return RPAREN;
"["	return LBRACKET;
"]"	return RBRACKET;
"{"	return LBRACE;
"}"	return RBRACE;
";"	return SEMI;
":"	return COLON;
","	return COMMA;
"&"	return AMPER;
"&&"	return LAND;
"?"	return QUES;
"++"	return PLUSPLUS;
"--"	return MINUSMINUS;
"+"	return PLUS;
"-"	return MINUS;
"#+"	return FPLUS;
"#-"	return FMINUS;
"!"	return BANG;
"="	return EQUALS;
"*="	return STAR_EQUALS;
"/="	return SLASH_EQUALS;
"%="	return PERCENT_EQUALS;
"+="	return PLUS_EQUALS;
"-="	return MINUS_EQUALS;
"<<="	return LSHIFT_EQUALS;
">>="	return RSHIFT_EQUALS;
"&="	return AMPER_EQUALS;
"^="	return XOR_EQUALS;
"|="	return OR_EQUALS;
"|"	return OR;
"||"	return LOR;
"^"	return XOR;
"=="	return EQ;
"!="	return NE;
"<"	return LT;
"<="	return LE;
">"	return GT;
">="	return GE;
"#=="	return FEQ;
"#!="	return FNE;
"#<"	return FLT;
"#<="	return FLE;
"#>"	return FGT;
"#>="	return FGE;
"<<"	return LSHIFT;
">>"	return RSHIFT;
"%"	return PERCENT;
"*"	return STAR;
"/"	return SLASH;
"#*"	return FSTAR;
"#/"	return FSLASH;
"#"	return FLOAT;
"##"	return FIX;
"~"	return COMPL;
"@"	return AT;

\/\*(.*\n)*.*\*\/	;
.	return yytext[0];

%%

void set_manifest (void)
  {
    yy_push_state (manifest); 
  }



