%{
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include "bc.h"

extern FILE *fp;
FILE * f1;
int yyerror (const char *s);
int yylex (void);
int yyparse (void);
void set_manifest (void);
extern int yydebug;
extern int yylineno;

int yyerror (const char * str)
  {
    fprintf (stderr, "error: %s line %d\n", str, yylineno);
    return 0;
  }

int yywrap ()
  {
    return 1;
  }

%}

%token <string> NAME 
%token NUM
%token AUTO
%token EXTRN
%token IF
%token ELSE
%token FOR
%token WHILE
%token REPEAT
%token SWITCH
%token DO
%token RETURN
%token BREAK
%token GOTO
%token NEXT
%token CASE
%token DEFAULT
%token LPAREN
%token RPAREN
%token LBRACKET
%token RBRACKET
%token LBRACE
%token RBRACE
%token SEMI
%token COLON
%token COMMA
%token AMPER
%token LAND
%token OR
%token LOR
%token XOR
%token QUES
%token PLUSPLUS
%token MINUSMINUS
%token PLUS
%token FPLUS
%token MINUS
%token FMINUS
%token BANG
%token EQUALS
%token STAR_EQUALS
%token SLASH_EQUALS
%token PERCENT_EQUALS
%token PLUS_EQUALS
%token MINUS_EQUALS
%token LSHIFT_EQUALS
%token RSHIFT_EQUALS
%token AMPER_EQUALS
%token XOR_EQUALS
%token OR_EQUALS
%token EQ
%token NE
%token LT
%token LE
%token GT
%token GE
%token FEQ
%token FNE
%token FLT
%token FLE
%token FGT
%token FGE
%token LSHIFT
%token RSHIFT
%token PERCENT
%token STAR
%token FSTAR
%token SLASH
%token FSLASH
%token FLOAT
%token FIX
%token COMPL
%token AT
%token STRING  /* double quotes, 4 characters to a word, NUL terminated */
%token BCD /* backtick, 1 to 6 bcd characters in a word, 0 padded */
%token CCONST /* tick. 1 to 4 characters in a word, 0 padded */
%token <string> MANIFEST /* manifest constant value */

  /* assignment has lowest precedence */

/* lowest to highest */
/* Assignments group right to left. */
%right EQUALS STAR_EQUALS SLASH_EQUALS PERCENT_EQUALS PLUS_EQUALS MINUS_EQUALS LSHIFT_EQUALS RSHIFT_EQUALS AMPER_EQUALS XOR_EQUALS OR_EQUALS
/* query grouping is right to left */
%right QUES
/* logical OR grouping not documented */
%left LOR
/* logical AND grouping not documented */
%left LAND
/* Relational grouping not documented */
%left EQ NE LT LE GT GE FEQ FNE FLT FLE FGT FGE 
/* Additive operators group left to right. */
%left PLUS MINUS FPLUS FMINUS
/* Multiplicative operators group from left to right. */
%left STAR SLASH PERCENT FSTAR FSLASH
/* bitwise operators group from left to right. */
%left OR
%left XOR
%left AMPER
/* shift operators group from left to right. */
%left LSHIFT RSHIFT
%right PLUSPLUS MINUSMINUS
/* Phony for distinguishing unary minus et al. */
%right UNARY
%left NAME CONSTANT ARRAY FUNC PEXPR
%precedence NO_ELSE
%precedence ELSE

%union
{
  word number;
  char * string;
}

%define parse.error verbose

%%

program :	definitions

definitions : /* empty */
	| definitions definition

definition : simple_external_definition
	| vector_external_definition
	| function_definition { close_function (); }
	| manifest

manifest : NAME EQUALS { set_manifest (); } MANIFEST { define_manifest ($1, $4); }

simple_external_definition : NAME ival_list SEMI

vector_external_definition : NAME LBRACKET RBRACKET ival_list SEMI
	| NAME LBRACKET constant RBRACKET ival_list SEMI


function_definition : NAME LPAREN { define_function ($1); } arg_list RPAREN { emit_function_entry (); } statement


arg_list : /* empty */
	| NAME { define_argument ($1); }
	| arg_list COMMA NAME { define_argument ($3); }

ival_list : /* empty */
	| ival
	| ival_list COMMA ival

ival : constant %prec CONSTANT
	| NAME

statement : AUTO name_constant_list SEMI
	| EXTRN name_list SEMI
	| NAME COLON statement 
	| CASE constant COLON statement
	| DEFAULT COLON statement
	| LBRACE RBRACE
	| LBRACE statement_list RBRACE
	| IF LPAREN rvalue RPAREN statement %prec NO_ELSE
	| IF LPAREN rvalue RPAREN statement ELSE statement 
	| WHILE LPAREN rvalue RPAREN statement
	| SWITCH LPAREN rvalue RPAREN LBRACE statement_list RBRACE
	| GOTO rvalue SEMI
	| RETURN SEMI
	| RETURN LPAREN rvalue RPAREN SEMI
	| BREAK SEMI
	| NEXT SEMI
	| REPEAT SEMI
	| DO statement WHILE LPAREN rvalue RPAREN SEMI
	| FOR LPAREN rvalue SEMI rvalue SEMI rvalue RPAREN statement
	| rvalue SEMI
	| SEMI

statement_list : statement
          | statement_list statement

name_list : NAME
	| name_list COMMA NAME

name_constant_list : name_constant
	| name_constant_list COMMA name_constant

name_constant : NAME { define_auto ($1, 1); }
	| NAME LBRACKET constant RBRACKET { define_auto ($1, yylval.number); }

rvalue :	LPAREN rvalue RPAREN %prec PEXPR
	| lvalue
	| constant
	| lvalue assign rvalue %prec EQUALS
	| inc_dec lvalue %prec UNARY
	| lvalue inc_dec %prec UNARY
	| unary rvalue %prec UNARY
	| AMPER lvalue %prec UNARY
	| rvalue binary rvalue
	| rvalue QUES rvalue COLON rvalue
	| rvalue LPAREN rvalue_list RPAREN %prec FUNC

rvalue_list : /* empty */
	| rvalue
	| rvalue_list COMMA rvalue

assign : EQUALS
	| STAR_EQUALS
	| SLASH_EQUALS
	| PERCENT_EQUALS
	| PLUS_EQUALS
	| MINUS_EQUALS
	| LSHIFT_EQUALS
	| RSHIFT_EQUALS
	| AMPER_EQUALS
	| XOR_EQUALS
	| OR_EQUALS

inc_dec : PLUSPLUS
	| MINUSMINUS

unary : FLOAT
	| FIX
	| COMPL
	| MINUS
	| FMINUS
	| BANG
	| AT

binary : OR
	| XOR
	| AMPER
	| EQ
	| NE
	| LT
	| LE
	| GT
	| GE
	| FEQ
	| FNE
	| FLT
	| FLE
	| FGT
	| FGE
	| LSHIFT
	| RSHIFT
	| MINUS
	| PLUS
	| FMINUS
	| FPLUS
	| PERCENT
	| STAR
	| SLASH
	| FSTAR
	| FSLASH
	| LAND
	| LOR

lvalue : NAME
	| STAR rvalue %prec UNARY
	| rvalue LBRACKET rvalue RBRACKET %prec ARRAY

constant : NUM
	| STRING
	| CCONST
	| BCD

	
